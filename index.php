<?php session_start(); ?>
<!DOCTYPE html>
<html lang ="en"><head>
<meta charset="utf-8">
<meta name = "description" content="Check Out C: Learn the C Programming Language"/>
<title>Check Out C</title>
<link href='/favicon.ico' rel='shortcut icon'>
<link rel="stylesheet" type="text/css" href="../css/Style.css" />
<link rel="stylesheet" type="text/css" href="../css/pure.css" />
<script src="../js/jquery-1.7.1.min.js"></script>
<script src="../js/jquery.mousewheel-min.js"></script>
<script src="../js/jquery.terminal-min.js"></script>
<link href="../css/jquery.terminal.css" rel="stylesheet"/>

</head>
<body>
	<div class ="header">
		<span id="logo"><a href="/">Check Out C</a></span>

       <?php 
       if($_SESSION["access_granted"]==false){
          $_SESSION["token"] = md5(uniqid(mt_rand(), true));
           echo '<form class="pure-form" id ="login" action ="handler.php" method ="POST">
           <fieldset>

	   <input type ="hidden" name="token" value ="';
	   echo $_SESSION["token"];
	   echo '">
           <input type="text" placeholder="Email or Username" name="email">
           <input type="password" placeholder="Password" name="password">

           <button type="submit" class="pure-button pure-button-primary" name = "loginButton">Sign in</button>
           </fieldset>
           </form>';}
           else{
            echo '<form class="pure-form" id ="login" action ="handler.php" method ="POST">
            <fieldset><button type="logout" class="pure-button pure-button-primary" name = "logoutButton">Logout</button></fieldset>
            </form>';


        }?>
        <span id="nav"><a href="/Lessons.php">Lessons </a>
            <?php if($_SESSION["access_granted"]==false){ 
                echo '<a href="/CreateAccount.php">Register </a></span>';}?>




            </div>
            <div class="content">
              <h1> Learn C</h1>
<p>The C language has been around for decades, a general purpose programming language which was developed at Bell Laboratories in 1972 by Dennis Ritchie,  is still one of the top 10 most popular languages. Software like System Programming, Windows, the Python interpeter, Git, and many more still use C. </p>

              <div id="starwarsterm" class="terminal" style="width: 500px; height: 230px;">
    <div class="terminal-output">
        <div>
            <div style="width: 100%;"> /* Hello World program */</div>
            <div style="width: 100%;"> #include &lt;stdio.h &gt;</div>
            <div style="width: 100%;"></div>
            <div style="width: 100%;">main()</div>
            <div style="width: 100%;">{</div>
            <div style="width: 100%;"></div>
            <div style="width: 100%;"></div>
            <div style="width: 100%;">printf(&quot;Hello World!&quot;);</div>
            <div style="width: 100%;"></div>
            <div style="width: 100%;"></div>
            <div style="width: 100%;"> }</div>
             <div style="width: 100%;"><br> </div>
              <div style="width: 100%;">Welcome! Type the following command compile your program:</div>
              <div style="width: 100%;color:yellow;">gcc hello.c </div>
        </div>
    </div>
    <span class="cursor inverted">&nbsp;</span><span></span>
        <textarea class="clipboard"></textarea>
    </div>
</div>
              

<script>
jQuery(function($, undefined) {
    $('#starwarsterm').terminal(function(command, term) {
        if (command !== 'gcc hello.c'&& command !== './a.out') {
            try {
                var result = window.eval(command);
                if (result !== undefined) {
                    term.echo(new String(result));
                }
            } catch(e) {
                term.error(new String(e));
            }
        } else if (command=='gcc hello.c'){
           term.echo('\n[[guib;#000;#ffff00]a.out]\n\n[[guib;#000;#00ee11]Awesome, your file has compiled. Now let\'s run that executable.\nType the follow command to run your program:\n\n./a.out]');
           
       }
       else if (command =='./a.out'){
	   term.echo('[[guib;#000;#ffff00]Hello World!]');
	   term.echo('\n[[guib;#000;#00ee11]Congratulations, you\'ve compile your first C Program.]');
	   term.echo('[[guib;#000;#00ee11]Check out the lessons or register for an account.]');
	   $("#present").show();

       }
   }, {
    greetings: '',
    name: 'js_demo',
    height: 200,
    prompt: 'checkoutc.com> '});
});</script>

<!--<h2>Why Learn C</h2>
<p>The C language has been around for decades, a general purpose programming language which was developed at Bell Laboratories in 1972 by Dennis Ritchie,  is still one of the top 10 most popular languages. Software like System Programming, Windows, the Python interpeter, Git, and many more still use C. </p>
-->
<span id="logo"><a href="/Lessons.php">Check Out the Lessons</a>
<a id="present"style="display:none;" href="/CreateAccount.php">Register </a></span>




</div>

<div class="footer">
  <li class="first">©2013 Check Out C</li>
  <li><a href="/">Home</a></li>
  <li><a href="/Lessons.php">Lessons</a></li>
  <li><a href="https://bitbucket.org/AlbertoRuis/webdev-checkoutc.com">SOURCE CODE</a></li>
</div>
</body>

</html>