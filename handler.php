<?php
session_start();


// handler.php
// handle comment posts, saving to MySQL and redirecting back to the list
require_once "Dao.php";

// define variables and set to empty values
$username = $email= $confirmEmail = $password = $confirmPassword ="";
$token="";
$error=false;

//Errors
$usernameErr = $emailErr =$confirmEmailErr= $passwordErr =$confirmPasswordErr= $websiteErr = "";
$length =0;

//If request is coming from the Register new Account Page.
  if (isset($_POST["registerButton"])) {
    
    //check's to see if token is present
    if(empty($_POST["token"])){
      header("Location:/index.php");
      
    }
    else if ($_POST["token"]!=$_SESSION["token"]){
    header("Location:/index.php");
    
    }
    if (empty($_POST["username"]))
    {
      $status = "Invalid username or password";
      $usernameErr = "Username is required";
      $error = true;
    }
    else
    {
    $length = strlen($_POST["username"]);
    if($length>20){
      $error = true;
      $usernameErr= "Username must be under 20 characters";
      $length=0;
    }
    else{
      $username = clean_input( $_POST["username"]);
      
      }
    }
    
    if (empty($_POST["email"]))
    {
      $emailErr = "Email is required";
      $error = true;
    }
    else
    {
      $length = strlen($_POST["email"]);
      
      if($length>256){
	$error =true;
	$emailErr = "Your email is too long";
      }
      else{
      $email = strtolower(clean_input($_POST["email"]));
      crypt
      if (!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/",$email))
	{
	$emailErr = "Invalid email format";
	$error = true;
      }
      
      }
    }
    
    if (empty($_POST["confirmEmail"]))
    {
      $confirmEmailErr = "Please confirm your email";
      $error = true;
    }
    else{
  
    $length = strlen($_POST["confirmEmail"]);
      
      if($length>256){
	$error =true;
	$confirmEmailErr = "Your email is too long";
      }
      else{
      $confirmEmail = strtolower(clean_input($_POST["confirmEmail"]));
      
      if($email != $confirmEmail)
      {
	$emailErr = "Your Emails don't match";
	$confirmEmailErr = "Your Emails don't match";
	$error = true;
      }}
    }
    
    if(empty($_POST["password"]))
    {
      $passwordErr = "Please enter a password";
      $error = true;
    }
    else
    {
    
    //raw password
      $password = $_POST["password"];
      $length = strlen($password);
      
      if($length <6){
      $error = true;
      $passwordErr = "Password too short";

      }
      else if($length >12){
      $error = true;
      $passwordErr = "Password too long";
      }
      else{
      //encrypted password
       $password = crypt($_POST["password"]);
        
      }
      
     
    }

    if(empty($_POST["confirmPassword"]))
    {
      $confirmPasswordErr = "Please confirm your password";
     $error = true;
    }
    else
    {
      $confirmPassword = $_POST["confirmPassword"];
      if(!isset($passwordErr)){
  
      if(crypt($confirmPassword,$password)!= $password)
      //if($confirmPassword!= $password)
	{
	$confirmPasswordErr = "Your passwords don't match";
	$error = true;
	}
      
      }

      }
        
     if($error==false){ 
       try {
      $dao = new Dao();
      
      $exist = $dao->doesUserExist(strtolower($username), strtolower($email));
      
      if($exist["email"]!=false){
      
      $error=true;
      $emailErr = "Sorry your account is already registered.";
      }
      if($exist["username"]!=false)
      {
      $error =true;
      $usernameErr = "Sorry that username is already taken";
      }
      if($error ==true){
      
      $_SESSION["error"] = $error;
      $_SESSION["username"] = $username;
      $_SESSION["usernameErr"] = $usernameErr;

      $_SESSION["password"] = $password;
      $_SESSION["passwordErr"] = $passwordErr;

      $_SESSION["confirmPassword"] = $confirmPassword;
      $_SESSION["confirmPasswordErr"] = $confirmPasswordErr;

      $_SESSION["email"] = $email;
      $_SESSION["emailErr"] = $emailErr;

      $_SESSION["confirmEmail"] = $confirmEmail;
      $_SESSION["confirmEmailErr"] = $confirmEmailErr;
      header("Location:/CreateAccount.php");
      
      }
      else {
      $dao->addUser($username,$email, $password);
       $_SESSION["error"] =false;
       $_SESSION["access_granted"]=true;
       $_SESSION["userID"]= $username;
	$_SESSION["username"] ="";
	$_SESSION["usernameErr"] ="";

	$_SESSION["password"] ="";
	$_SESSION["passwordErr"] ="";

	$_SESSION["confirmPassword"] ="";
	$_SESSION["confirmPasswordErr"] ="";

	$_SESSION["email"] ="";
	$_SESSION["emailErr"] ="";

	$_SESSION["confirmEmail"] ="";
	$_SESSION["confirmEmailErr"] ="";
	 header("Location:/index.php");

      }
      
    } catch (Exception $e) {
      var_dump($e);
      die; 
    }}
    
    else{
       $_SESSION["error"] = $error;
      $_SESSION["username"] = $username;
      $_SESSION["usernameErr"] = $usernameErr;

      $_SESSION["password"] = $password;
      $_SESSION["passwordErr"] = $passwordErr;

      $_SESSION["confirmPassword"] = $confirmPassword;
      $_SESSION["confirmPasswordErr"] = $confirmPasswordErr;

      $_SESSION["email"] = $email;
      $_SESSION["emailErr"] = $emailErr;

      $_SESSION["confirmEmail"] = $confirmEmail;
      $_SESSION["confirmEmailErr"] = $confirmEmailErr;
       header("Location:/CreateAccount.php");
    
    }
    
    
       
      
  }
  
//If a user is logging in
  if (isset($_POST["loginButton"])){
  
   if (empty($_POST["email"]))
    {
      $error = true;
    }
    else
    {
      $length = strlen($_POST["email"]);
      
      if($length>256){
	$error =true;
      }
      else{
      $email = strtolower(clean_input($_POST["email"]));
      if (!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/",$email))
  {
  $emailErr = "Invalid email format";
  $error=true;
  }
      }
    }
    
     if(empty($_POST["password"]))
    {
      $error = true;
    }
    else
    {
    
    //raw password
      $password = $_POST["password"];
      $length = strlen($password);
      
      if($length <6){
      $error = true;

      }
      else if($length >12){
      $error = true;
      }}
      if ($error){
      
      $_SESSION["access_granted"]=false;
       $_SESSION["userID"]= "";
	$_SESSION["username"] ="";
	$_SESSION["usernameErr"] ="";

	$_SESSION["password"] ="";
	$_SESSION["passwordErr"] ="";

	$_SESSION["confirmPassword"] ="";
	$_SESSION["confirmPasswordErr"] ="";

	$_SESSION["email"] ="";
	$_SESSION["emailErr"] ="";

	$_SESSION["confirmEmail"] ="";
	$_SESSION["confirmEmailErr"] ="";
	 header("Location:/index.php");
      
      
      }else{
     
       try {
      $dao = new Dao();
      $right = $dao->checkPassword($email, $password);
      
      if(right){
      
      
       $_SESSION["error"] =False;
       $_SESSION["access_granted"]=True;
       $_SESSION["userID"]= $username;
	$_SESSION["username"] ="";
	$_SESSION["usernameErr"] ="";

	$_SESSION["password"] ="";
	$_SESSION["passwordErr"] ="";

	$_SESSION["confirmPassword"] ="";
	$_SESSION["confirmPasswordErr"] ="";

	$_SESSION["email"] ="";
	$_SESSION["emailErr"] ="";

	$_SESSION["confirmEmail"] ="";
	$_SESSION["confirmEmailErr"] ="";
	 header("Location:/Lessons");
      
      }
      else{
      
      $_SESSION["access_granted"]=False;
       $_SESSION["userID"]= "";
	$_SESSION["username"] ="";
	$_SESSION["usernameErr"] ="";

	$_SESSION["password"] ="";
	$_SESSION["passwordErr"] ="";

	$_SESSION["confirmPassword"] ="";
	$_SESSION["confirmPasswordErr"] ="";

	$_SESSION["email"] ="";
	$_SESSION["emailErr"] ="";

	$_SESSION["confirmEmail"] ="";
	$_SESSION["confirmEmailErr"] ="";
	 header("Location:/index.php");
      
      }
      
 
    }
    catch (Exception $e) {
      var_dump($e);
      die; 
    }
        
  
  
  }}
 
 if(isset($_POST["logoutButton"])){
 
 session_destroy();
 header("Location:index.php");
 }
 
 function doWeStoreSession($errorcode){
 
  if ($errorcode==true){

      $_SESSION["error"] = $errorcode;
      $_SESSION["username"] = $username;
      $_SESSION["usernameErr"] = $usernameErr;

      $_SESSION["password"] = $password;
      $_SESSION["passwordErr"] = $passwordErr;

      $_SESSION["confirmPassword"] = $confirmPassword;
      $_SESSION["confirmPasswordErr"] = $confirmPasswordErr;

      $_SESSION["email"] = $email;
      $_SESSION["emailErr"] = $emailErr;

      $_SESSION["confirmEmail"] = $confirmEmail;
      $_SESSION["confirmEmailErr"] = $confirmEmailErr;
      header("Location:/CreateAccount.php");
             }
  else{
       
         $_SESSION["error"] =false;
	$_SESSION["username"] ="";
	$_SESSION["usernameErr"] ="";

	$_SESSION["password"] ="";
	$_SESSION["passwordErr"] ="";

	$_SESSION["confirmPassword"] ="";
	$_SESSION["confirmPasswordErr"] ="";

	$_SESSION["email"] ="";
	$_SESSION["emailErr"] ="";

	$_SESSION["confirmEmail"] ="";
	$_SESSION["confirmEmailErr"] ="";
	 header("Location:/index.php");
	 
       }
 
 }  
    

function clean_input($data)
{
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

?>
