<?php

/* Program: Dao.php
 * Author: Alberto Ruis
 * Date: Nov. 9th 2013
 * Purpose: A php class to connect to our database.
 */

class Dao {

  private $host = "db501332634.db.1and1.com";
  private $db = "db501332634";
  private $user = "dbo501332634";
  private $pass = "P7yxJ8wV";

  public function getConnection () {
    $dbo = new PDO("mysql:host={$this->host};dbname={$this->db}", $this->user,$this->pass);
    $dbo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    return $dbo;
  }


  public function getUsers () {
   
   try{
     $conn = $this->getConnection();
     $users = $conn->query("SELECT * FROM user");
     return $users;

   }
   catch(PDOException $ex){

    return false;
   }
     
  }
  
  public function checkPassword($email, $password){
  
  try{
       $conn = $this->getConnection();

        $emailQuery =
        "SELECT * FROM user WHERE email=:email;";
        $usernameQuery = 
        "SELECT * FROM user WHERE email=:email;";
        
        
        $q = $conn->prepare($emailQuery);
        $q->bindParam(':email', $email);
        $q->execute();
        $result = reset($q->fetchAll());
        if(!result){
        
	   $q = $conn->prepare($usernameQuery);
	   $q->bindParam(':username', $email);
           $q->execute();
        $result = reset($q->fetchAll());
	  if(!result){
	    return False;
	  }
	  else{
	  $userPass = $result["password"];
	  
	 if(crypt($password,$userPass)==$userPass)
	 {
	  return True;
	 }

        }}
        else{
        
        $userPass = $result["password"];
        if(crypt($password,$userPass)==$userPass)
        {
        return True;
        }
        else{
        return False;
        }}


     } 
     catch(PDOException $ex){

    return False;
   }
  
  
  }
  
 public function doesUserExist( $username, $email){
 
    try{
       $conn = $this->getConnection();

        $emailQuery =
        "SELECT username, email FROM user WHERE email=:email;";
        $usernameQuery =
        "SELECT username, email FROM user WHERE LOWER(username) =:username;";
        
        $q = $conn->prepare($emailQuery);
        $q->bindParam(':email', $email);
        $q->execute();
        $result["email"] = $q->fetchAll();
        
        $q = $conn->prepare($usernameQuery);
        $q->bindParam(':username', $username);
        
        $q->execute();
        $result["username"] = $q->fetchAll();
        
    
    return $result;

     } 
     catch(PDOException $ex){

    return False;
   }
 
 }

  public function addUser($username,$email, $password){

    try{
       $conn = $this->getConnection();

        $saveQuery =
        "INSERT INTO user
        (username,email, password)
        VALUES
        (:username, :email, :password)";
    $q = $conn->prepare($saveQuery);
    $q->bindParam(":username", $username);
    $q->bindParam(":email", $email);
    $q->bindParam(":password", $password);
    $q->execute();
     } 
     catch(PDOException $ex){

    return False;
   }


  }
} // end Dao
?>