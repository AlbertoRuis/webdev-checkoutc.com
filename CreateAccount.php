<?php session_start(); 
$username = $email= $confirmEmail = $password = $confirmPassword ="";
$usernameErr = $emailErr =$confirmEmailErr= $passwordErr = $confirmPasswordErr= $websiteErr = "";
if ($_SESSION["access_granted"]){
header('Location: http://checkoutc.com/lessons');
}

  if ($_SESSION["error"]) {
    
    $username = $_SESSION["username"] ;
    $usernameErr= $_SESSION["usernameErr"];

    $password ="";
    $passwordErr= $_SESSION["passwordErr"];

    $confirmPassword = "";
    $confirmPasswordErr = $_SESSION["confirmPasswordErr"];

    $email=  $_SESSION["email"];
    $emailErr = $_SESSION["emailErr"];

     $confirmEmail=$_SESSION["confirmEmail"];
    $confirmEmailErr = $_SESSION["confirmEmailErr"];
  }
?>
<!DOCTYPE html>
<html lang ="en"><head>
<meta charset="utf-8">
<meta name = "description" content="Check Out C: Learn the C Programming Language"/>
<title>Check Out C</title>
<link href='/favicon.ico' rel='shortcut icon'>
<link rel="stylesheet" type="text/css" href="../css/Style.css" />
<link rel="stylesheet" type="text/css" href="../css/pure.css" />

</head>
<body>
	<div class ="header">
		<span id="logo"><a href="/">Check Out C</a></span>

			
			 <?php 
       if($_SESSION["access_granted"]==false){
          $_SESSION["token"] = md5(uniqid(mt_rand(), true));
           echo '<form class="pure-form" id ="login" action ="handler.php" method ="POST">
           <fieldset>

	   <input type ="hidden" name="token" value ="';
	   echo $_SESSION["token"];
	   echo '">
           <input type="text" placeholder="Email or Username" name="email">
           <input type="password" placeholder="Password" name="password">

           <button type="submit" class="pure-button pure-button-primary" name = "loginButton">Sign in</button>
           </fieldset>
           </form>';}
           else{
            echo '<form class="pure-form" id ="login" action ="handler.php" method ="POST">
            <fieldset><button type="logout" class="pure-button pure-button-primary" name = "logoutButton">Logout</button></fieldset>
            </form>';


        }?>
<span id="nav"><a href="/Lessons.php">Lessons </a>
<?php if($_SESSION["access_granted"]==false){ 
echo '<a href="/CreateAccount.php">Register </a></span>';}?>




	</div>
	<div class="content">
		<h1>Sign-Up and Learn C!</h1>
		<?php
        if($_SESSION["error"]){
        echo "<span class=\"error\">*Denotes Required Field</span>";}?>
		<form class="pure-form pure-form-aligned" action ="handler.php" method ="POST">
    <fieldset>
        <div class="pure-control-group">
            <label for="username">Username</label>
            <input id="username" type="text" name="username" placeholder ="Enter Username" maxlength="20" value=<?php echo $username;?>> 
            <span class="error"> <?php echo $usernameErr;?></span>
        </div>

        <div class="pure-control-group">
            <label for="password">Password</label>
            <input id="password" type="password" name="password" placeholder=" 6-12 Character Password" maxlength="12" >
            <span class="error"> <?php echo $passwordErr;?></span>
        </div>
         <div class="pure-control-group">
            <label for="confirmPassword"></label>
            <input id="confirmPassword" type="password" name="confirmPassword" placeholder="Confirm Password" maxlength="12">
             <span class="error"> <?php echo $confirmPasswordErr;?></span>
        </div>

        <div class="pure-control-group">
            <label for="email">Email Address</label>
            <input id="email" type="email" name="email" placeholder="Email@example.com" value=<?php echo $email;?>>

              <span class="error"> <?php echo $emailErr;?></span>
        </div>
        <div class="pure-control-group">
            <label for="confirmEmail"></label>
            <input id="confirmEmail" type="email" name="confirmEmail" placeholder=" Confirm Email" value=<?php echo $confirmEmail;?>>
             <span class="error"> <?php echo $confirmEmailErr;?></span>
        </div>
        <div class="pure-controls">
            <button type="submit" class="pure-button pure-button-primary" name="registerButton">Submit</button>
        </div>
    </fieldset>
</form>



		
	</div>

	<div class="footer">
		<li class="first">©2013 Check Out C</li>
		<li><a href="/">Home</a></li>
		<li><a href="/Lessons.php">Lessons</a></li>
		<li><a href="https://bitbucket.org/AlbertoRuis/webdev-checkoutc.com">SOURCE CODE</a></li>
	</div>
</body>

</html>
